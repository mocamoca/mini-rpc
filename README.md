# MINI-RPC

## rpc-provider：初始化

### 1，通过springboot的自动配置注解，获取注册中心地址及端口并生成一个服务提供者对象，注入spring容器

### 2，服务提供者对象实现BeanPostProcess，BeanInitialize即服务注入容器的前置处理，和生成处理

### 3，前置处理中将服务发布到注册中心（构造中获取）

### 4，后置处理中启动服务提供者服务，并记录当前提供服务

## rpc-consumer:初始化

### 1，RpcConsumerPostProcessor实现BeanFactory的前置处理，

- 同时需要实现ApplicationContenxtAware，ClassLoaderAware。即获取容器，和类加载器
- 1，前置处理器中获过滤所有BD，查找是否存在需要消费远程服务的属性：@RpcRef。有则根据该接收接口生成一个BD,并指定初始化方法，与默认属性。注册到spring中，交有spring管理。该DB指定为工厂bean。
- 2，通过注册中心提供的api获取，给代理对象

### 2，消费对象如何生成：FactoryBean生成服务提供者的动态代理对象，作为消费对象

- 2，实现上一步的FactoryBean和初始化步骤。初始化即生成消费服务的动态代理对象。
- 3，在代理对象的代理方法处理中，统一编写请求远程服务功能。

## rpc-registry

### 注册中心

- CAP理论

	- ZK:CP

		- raft协议：LEAD-FOLLOWER模式，选举期间不服务可用

	- EURAKA:AP

		- 主从模式，心跳方式从主服务中同步信息，同步过程中，主挂了，从则无法同步，失去一致性。但持续可用。

## rpc-protocol

### Decoder&Encoder

### 序列化

## 工作流程

### 1，注册中心启动

### 发起RPC请求：消费者消费服务

- 消费者调用远程服务，实际是通过RpcReferenceBean的getObject生成后注入的代理对象。

	- 消费者工厂Bean：

		- 1，代理接口，服务提供者接口
		- 2，注册中心的地址，类型，端口，超时
		- 3，动态代理对象：包含注册中心提供的服务

- 2，请求处理：统一在代理对象Invoke中，请求远程服务的方法。

	- 1，构造请求数据并序列化，
	- 2，构造请求协议报文
	- 3，构造消费者：创建Netty客户端，创建通用协议编码与解码
	- 4，消费者发起请求

		- 1，发现服务：从注册中心拉取服务提供者

			- 可优化：无需每次从拉去，

		- 2，选取提供者：自定义负载均衡算法
		- 3，发起

### 响应Rpc请求：服务提供者响应请求

- 1，服务端接受到请求后，按照bound流程先解析协议

	- 把Byte->自定义Protocol

- 2，RpcRequestHander实现InBound，创建线程池，处理请求处理请求
- 3，从本地发布的ServiceMap获取本地服务，通过反射调用。返回
- 4，按照OutBound流程编码协议

	- Protocaol->Byte[]

*XMind - Trial Version*