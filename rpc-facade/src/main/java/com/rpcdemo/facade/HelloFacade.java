package com.rpcdemo.facade;

public interface HelloFacade {
    String hello(String name);
}
