package com.yangm.consumer.controller;

import com.rpcdemo.facade.HelloFacade;
import com.yangm.consumer.annotation.RpcReference;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
public class HelloController {

    @SuppressWarnings({"SpringJavaAutowiredFieldsWarningInspection", "SpringJavaInjectionPointsAutowiringInspection"})
    @RpcReference(registryAddress = "127.0.0.1:2181",timeout = 10000)
    private HelloFacade helloFacade;

    @RequestMapping(value = "/hello",method = RequestMethod.GET)
    public String hello(){
        return  helloFacade.hello("name");

    }

}
