package com.yangm.consumer;

import com.yangm.core.MiniRpcFuture;
import com.yangm.core.MiniRpcRequest;
import com.yangm.core.MiniRpcRequestHolder;
import com.yangm.core.MiniRpcResponse;
import com.yangm.protocol.MiniRpcProtocol;
import com.yangm.protocol.MsgHeader;
import com.yangm.protocol.MsgType;
import com.yangm.protocol.ProtocolConst;
import com.yangm.serialization.SerializationTypeEnum;
import com.yangm.registry.RegistryService;
import io.netty.channel.DefaultEventLoop;
import io.netty.util.concurrent.DefaultPromise;
import lombok.extern.slf4j.Slf4j;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.util.concurrent.TimeUnit;

@Slf4j
public class RpcInvokerProxy implements InvocationHandler {

    private final String serviceVersion;
    private final long timeout;
    private final RegistryService registryService;

    public RpcInvokerProxy(String serviceVersion, long timeout, RegistryService registryService) {
        this.registryService = registryService;
        this.timeout = timeout;
        this.serviceVersion = serviceVersion;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args)throws Throwable  {
        MiniRpcProtocol<MiniRpcRequest> rpcProtocol = new MiniRpcProtocol<>();
        MsgHeader header = new MsgHeader();
        long requestId = MiniRpcRequestHolder.REQUEST_ID_GEN.incrementAndGet();
        header.setMagic(ProtocolConst.MAGIC);
        header.setVersion(ProtocolConst.VERSION);
        header.setRequestId(requestId);
        header.setSerialization((byte) SerializationTypeEnum.HESSIAN.getType());
        header.setMsgType((byte) MsgType.REQUEST.getType());
        header.setStatus((byte) 0x1);
        rpcProtocol.setHeader(header);

        MiniRpcRequest request = new MiniRpcRequest();
        request.setServiceVersion(this.serviceVersion);
        request.setMethodName(method.getName());
        request.setClassName(method.getDeclaringClass().getName());
        request.setParameterTypes(method.getParameterTypes());
        request.setParams(args);

        rpcProtocol.setBody(request);

        RpcConsumer rpcConsumer = new RpcConsumer();
        MiniRpcFuture<MiniRpcResponse> future = new MiniRpcFuture<>(new DefaultPromise<>(new DefaultEventLoop()), timeout);

        MiniRpcRequestHolder.REQUEST_MAP.put(requestId, future);
        rpcConsumer.sendRequest(rpcProtocol, this.registryService);

//        // TODO hold request by ThreadLocal
//        while (future.getPromise().get(future.getTimeout(), TimeUnit.MILLISECONDS)==null){
//            log.info("lalalall");
//        }

        return future.getPromise().get(future.getTimeout(), TimeUnit.MILLISECONDS).getData();
    }
}
