package com.yangm.provider.service;

import com.rpcdemo.facade.HelloFacade;
import com.yangm.provider.anotation.RpcService;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RpcService(serviceInterFace = HelloFacade.class)
public class HelloServiceImpl implements HelloFacade {
    @Override
    public String hello(String name) {

        log.info("HelloServiceImpl----------->{}", name);
        return name+" HelloServiceImpl";
    }
}
