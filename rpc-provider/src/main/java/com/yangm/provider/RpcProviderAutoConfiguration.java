package com.yangm.provider;

import com.yangm.core.RpcProperties;
import com.yangm.core.RegistryType;
import com.yangm.registry.RegistryFactory;
import com.yangm.registry.RegistryService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.annotation.Resource;

@Configuration
@EnableConfigurationProperties(RpcProperties.class)
@Slf4j
public class RpcProviderAutoConfiguration {

    @Resource
    private RpcProperties properties;


    @Bean
    public RpcProvider init() {
        log.info("config ags:{},{},{}",properties.getServicePort(),properties.getRegistryAddr(),properties.getRegistryType());
        RegistryType registryType = RegistryType.valueOf(properties.getRegistryType());
        RegistryService registryService = null;
        try {
            registryService = RegistryFactory.getInstance(properties.getRegistryAddr(), registryType);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new RpcProvider(properties.getServicePort(), registryService);
    }
}
