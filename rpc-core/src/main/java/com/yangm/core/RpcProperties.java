package com.yangm.core;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Data
@ConfigurationProperties(prefix = "rpc")
public class RpcProperties {
    private int servicePort=2981;
    private String registryAddr="127.0.0.1";
    private String registryType;
}
