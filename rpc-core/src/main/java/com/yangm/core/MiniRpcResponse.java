package com.yangm.core;

import lombok.Data;

import java.io.Serializable;

@Data
public class MiniRpcResponse implements Serializable {
    private Object data;
    private String msg;
}
