package com.yangm.core;

public enum RegistryType {
    ZOOKEEPER, EUREKA
}
