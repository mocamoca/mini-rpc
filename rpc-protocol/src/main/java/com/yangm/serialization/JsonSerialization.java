package com.yangm.serialization;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.Arrays;

@Slf4j
public class JsonSerialization implements RpcSerialization {

    public static final ObjectMapper MAPPER;

    static {
        MAPPER = generateMapper(JsonInclude.Include.ALWAYS);
    }

    private static ObjectMapper generateMapper(JsonInclude.Include include) {
        ObjectMapper mapper = new ObjectMapper();
        mapper.setSerializationInclusion(include);
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        mapper.configure(DeserializationFeature.FAIL_ON_NUMBERS_FOR_ENUMS, true);
        mapper.setDateFormat(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"));

        return mapper;
    }

    @Override
    public <T> byte[] serialize(T obj) throws IOException {
        byte[] bytes = obj instanceof String ? ((String) obj).getBytes()
                : MAPPER.writeValueAsString(obj).getBytes(StandardCharsets.UTF_8);
        log.info("serialize after ...{}", Arrays.toString(bytes));

        return bytes;
    }

    @Override
    public <T> T deserialize(byte[] data, Class<T> clz) throws IOException {
        log.info("deserialize before ...{}", Arrays.toString(data));

        return MAPPER.readValue(Arrays.toString(data), clz);
    }
}
