package com.yangm.serialization;

public class SerializationFactory {

    public static RpcSerialization getRpcSerialization(byte serialType) {
        SerializationTypeEnum type = SerializationTypeEnum.findByType(serialType);
        switch (type) {
            case JSON:
                return new JsonSerialization();
            case HESSIAN:
                return new HessianSerialization();
            default:
                throw new IllegalArgumentException("serialization type is illagal," + serialType);
        }
    }
}
